import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message, MessageData } from './message.model';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class MessageService {

  constructor(private http: HttpClient) {}

  getMessages() {
    return this.http.get<Message[]>('http://localhost:8000/messages').pipe(
      map(response => {
        return response.map(messageData => {
          return new Message(
            messageData.author,
            messageData.message,
            messageData.image,
          );
        });
      })
    )
  }


  createMessage(messageData: MessageData) {
    const formData = new FormData();

    Object.keys(messageData).forEach(key => {
      if(messageData[key] !== null) {
        formData.append(key, messageData[key]);
      }
    });

    return this.http.post(`http://localhost:8000/messages`, messageData);
  }
}
