import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageData } from '../shared/message.model';
import { NgForm } from '@angular/forms';
import { MessageService } from '../shared/message.service';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.sass']
})
export class NewMessageComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  constructor(private messageService: MessageService ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const messageData: MessageData = this.form.value;
    this.messageService.createMessage(messageData).subscribe();
  }
}
