const fs = require('fs').promises;

const filename = './db.json';
let data = [];

module.exports = {
  async init() {
    try {
      const fileContents = await fs.readFile(filename);
      data = JSON.parse(fileContents.toString());
    } catch (e) {
      data = [];
    }
  },

  getMessages() {
    return data;
  },

  addMessage(message) {
    data.push(message);
    return fs.writeFile(filename, JSON.stringify(data, null, 2));
  }
}